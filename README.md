Loom: Unifying Client-Side Web Technologies in a Single Programming Language
============================================================================

Abstract
--------

Modern client-centred web applications typically depend on a set of complementary languages to control different layers of abstraction in their interfaces: the behaviour, structure, and presentation layers (in order, traditionally: JavaScript, HTML, and CSS).
Applications with dynamic interfaces whose structure and presentation depend on the data and state of the application require tight links between such layers; however, communicating between them is often non-trivial or simply cumbersome, mainly because they are effectively distinct languages—each with a specific way of being interacted with.

Numerous technologies have been introduced in an attempt to simplify the interaction between the multiple layers; their main focus so far, however, regards the communication between structure and behaviour—leaving room for improvement in the field of presentation.

This dissertation presents Loom: a novel reactive programming language that unifies the enunciated abstraction layers of a client-side web application.
Loom allows the specification of an interface's structure and presentation in a declarative, data-dependent, and reactive manner by means of signals—values that change over time—inspired by the field of functional reactive programming: reactive meaning that when the structure and presentation of an interface depend on application-data, changes to said data cause an automatic update of the application's interface.

We provide an implementation of the language's compiler that allows the creation of interfaces with performance comparable to that of most existent frameworks.

Download
--------

Latest PDF available at: https://gitlab.com/loom-lang/msc-thesis/builds/artifacts/master/raw/thesis.pdf?job=compile_pdf

Build
-----

Compile PDF with: `latexmk -pdf -outdir=build thesis.tex`
