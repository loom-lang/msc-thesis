%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Related Work                                                               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Related Work}
\label{cha:related-work}

Throughout this chapter we expose some of the technologies, techniques and overall work that is related, in one way or another, to the creation of a unified web language focused on client-centred applications.
In specific, we dive into the realm of web template engines, which provide useful insight on how \gls{html} values that depend on data can be specified; we look into \gls{css} preprocessors, which provide an extension to \gls{css} style sheets with interesting useful semantics; we explore already existent languages and frameworks that bridge different web technologies, explaining how these bridges are built; we discuss modularity in the context of the web and its implications; and finally, we present the notion of reactive programming, discussing why it is being adopted in the web, and what techniques are used to make some of its aspects more efficient.

%==============================================================================%
% Background and Foundation                                                    %
%==============================================================================%

\section{Background and Foundations}
\label{sec:related-work}

%------------------------------------------------------------------------------%
% Web Template Engines                                                         %
%------------------------------------------------------------------------------%

\subsection{Web Template Engines}
\label{subs:template-engines}

\gls{html} was created with the idea of static documents in mind; as such, it is not suited for the definition of the structure of nowadays interfaces that, more often than not, depend on some sort of external data.
In order to work around the limitations of \gls{html}, web template engines were created: they use a processor that allows them to generate \gls{html} documents, whilst possibly being parametrised.
These engines have been widely used on the server side of applications in order to generate server-side dynamic structures that depend on some external data, such as content from an \gls{html} form or from the state of the server.
More recently, they began being applied in the client-side, thus being able to produce client-side dynamic structures.

Client-side dynamic structures change their content on the occurrence of some event, such as time or user interaction (e.g.\ click of a button).
These sort of changes occur by means of \gls{dom} manipulation, only made possible with the aid of a programming language with access to the \gls{dom} \gls{api} (typically JavaScript).
Template engines started to be adopted on the client side of applications in order to avoid the specification of a web page's dynamic elements in nothing but JavaScript, which is often either verbose, or based on the creation of \gls{html} elements via strings.\footnote{With this being said, there are still some viable alternatives based solely on JavaScript, of which HyperScript~\cite{hyperscript} is a prime example.}
As such, engines must typically offer ways of being integrated with JavaScript---providing functions such as \code{render}, which typically receive data as arguments and produce the rendered template as an \gls{html} string.

\gls{ejs}~\cite{ejs}, Pug~\cite{pug}, and Handlebars~\cite{handlebars} are examples of popular template engines that integrate well with JavaScript; the study of these engines was useful in order to understand the main features of languages tailored to allow the definition of dynamic structures---which Loom supports. Bellow we find an example of how a template may look like (using Pug):
\begin{lstlisting}[language=pug]
#todo-app
  h1 Todos
  input.new-task(placeholder="Add a task")
  if tasks.length > 0
    ul.tasks-list
      each task in tasks
        li.task
          input.task-done(type="checkbox" checked=task.done)
          span.task-text=task.text
  else
    #no-tasks No tasks to show.
\end{lstlisting}

Other engines typically support a similar set of features.
The presented example shows some of the most common features of these engines: the usage of data variables (\code{tasks} array in the example) allows the definition of templates that vary depending on the application's data; loops and conditionals allow the specification of the template logic, where what is shown depends on the data of the application.

Other common features of template engines include syntax simplifications for common operations (in the Pug example \code[pug]{\#todo-app} is equivalent to \code[pug]{div(id="todo-app")} and \code{li.task} to \code[pug]{li(class="task")}; as previously seen, Loom borrowed this idea); and inheritance, where a template may extend another.

%------------------------------------------------------------------------------%
% CSS Preprocessors                                                            %
%------------------------------------------------------------------------------%

\subsection{CSS Preprocessors}
\label{subs:css-preprocessors}

\gls{css} preprocessors are tools that, provided code written in some specified language, compile it to \gls{css} --- they have become widely popular among web developers as a way of bypassing some of the limitations of plain \gls{css}~\cite{css-preprocessors}.
\gls{sass}~\cite{sass}, Less~\cite{less}, and Stylus~\cite{stylus} are examples of popular \gls{css} preprocessors~\cite{stylus-less-sass}.

The study of existent \gls{css} preprocessors was important, in our context, in order to understand what a language that features \gls{css} values as first-class citizens is able to provide ``out-of-the-box'', as well as what constructs must be added to the language in order to support some of the most useful/popular features of current preprocessors; as such, this section provides a concise description of the main capabilities of current popular \gls{css} preprocessors.

One of the main reasons behind developers' use of \gls{css} preprocessors instead of plain \gls{css} is the ability to declare identifiers. Identifiers naturally eliminate the common need for repeating \gls{css} literal values throughout a style sheet---such as colours, paddings, or widths; they are the simplest example of a feature that is automatically available in a programming language that supports \gls{css} values.

\gls{css} preprocessors further allow arithmetic and the use/definition of other functions.
They often support multiple data types: from numbers (e.g. \code{17}, \code{4em}, \code{18px}), strings, and colours (e.g. \code{red}, \code{\#ccc}, \code{rgb(0,0,100)}) to lists of values, separated by spaces or commas (e.g. \code{10px 0 5px 1px}, \code{Arial, Helvetica, sans-serif}).
These functionalities should also appear naturally in a programming language containing \gls{css} values, although it might not be trivial to determine appropriate ways of specifying the values of \gls{css} properties (due to the nature of \gls{css} syntax, e.g.\ some lists of values are represented with commas, whilst others have spaces)---Loom currently requires strings to be used to represent most \gls{css} values; in the future, more thinking has to go into how to better integrate \gls{css} with the language.
As an example, consider the following style sheet written in \gls{sass}:
\begin{lstlisting}[language=css]
$fontColor: black;
#content {
  color: $fontColor;
  a {
    color: lighten($fontColor, 20%);
    &:hover { color: lighten($fontColor, 10%); }
  }
}
\end{lstlisting}
Other preprocessors typically support a similar set of features, albeit with different syntax.
In addition to allowing the usage of identifiers (\code{\$fontColor} in the example) and functions (in the example, \code{lighten} is a function that takes a colour and a percentage as arguments and produces a new lighter colour), most preprocessors offer the ability to nest rules.
The example presents such a nesting: the styles for \code{a} are applied when \code{a} is a child of \code{\#content}.
Furthermore, since \code{\&} references the parent selector, \code{\&:hover} represents the selector \code{a:hover}.
The usage of nested rules when writing style sheets has become popular due to its intrinsic resemblance to the way in which \gls{html} is specified.
Indeed, by looking at style sheets written in such a way, developers are more easily able to discern a rough approximation of the structure of an interface. In Loom's case, it is also arguably the most natural way of composing a style sheet---compare it with the scoped \gls{css} style sheets exposed in \cref{sec:css-values}.

Other useful features of popular \gls{css} preprocessors include the ability to extend rules, to deal with imports, and to interpolate selectors and property names.
Rule extension makes it possible for a rule to extend all properties of another rule, whilst adding new properties; Loom supports this via the splats operator (\code{...}) in \gls{css} values.
Imports are supported natively by \gls{css}; preprocessors, however, extend their functionality in order to import other files written within the same preprocessor language.
In Loom, importing in unrelated with \gls{css}; it is a concern of the module system which allows any value in Loom to be imported or exported (obviously including \gls{css} values).
Interpolating selectors and property names refers to the usage of variables inside them (e.g.~\code{p.\#\{\$name\}\{margin-\#\{\$dir\}:10px;\}}).
Although not introduced in \cref{sec:first-class-css}, Loom supports the interpolation of property names but not of selectors: selectors are parsed by Loom and syntactically checked by its parser (see \cref{sec:future-directions} as to why this may be of importance in the future).

%------------------------------------------------------------------------------%
% CSS in JavaScript                                                            %
%------------------------------------------------------------------------------%

\subsection{CSS in JavaScript}
\label{subs:css-in-js}

Christopher Chedeau's talk---``React: CSS in JS''---introduced in \cref{sec:reactive-presentation}, enunciates a set of problems encountered when dealing with \gls{css} in a large scale: problems which aren't solved by the adoption of \gls{css} preprocessors.

Chedeau thus proposes an approach where JavaScript is used to define the presentation of elements of an interface.
This proposal gave rise to the appearance of a plethora of JavaScript libraries~\cite{css-in-js-list}: Radium~\cite{radium} and Aphrodite~\cite{aphrodite} being two commonly mentioned ones.

Most of the created libraries attempt to provide an easy way for styles to be defined in JavaScript, whilst avoiding some of the issues with Chedeau's approach: it is hard/impossible to support certain \gls{css} features with inline styles.
Some of these features include: media queries, browser states (\gls{css} pseudo-classes such as \code{:hover}, \code{:focus}, \code{:visited}), and keyframes.

As far as we are aware, there are two main types of ``CSS in JS'' libraries, in regard to their implementation: those that attempt to follow Chedeau's proposal and implement most styles as inline (this is the case for Radium); and those that, keeping the idea of inline-styles in mind, compile the styles generated in JavaScript to actual \gls{css}: this is what Aphrodite and Fela~\cite{fela} do behind the scenes---from where Loom got its inspiration.

The latter approach has the advantage of having no limits regarding what may be supported from \gls{css}: since the JavaScript compiles down do \gls{css}, all \gls{css} functionality should be available to the user.

%------------------------------------------------------------------------------%
% Bridging Web Technologies                                                    %
%------------------------------------------------------------------------------%

\subsection{Bridging Web Technologies}
\label{subs:bridging-technologies}

Creating a bridge between some of the technologies that are common on web applications is something that has already been done, and is still being done by a number of different languages and frameworks.
This section presents some of them, explaining their concerns and what can be learned (in the context of this dissertation) from what they provide.
We also show that none of them attempt to offer first-class support for style sheets in order to support reactive data-dependent presentations.

% Opa --------------------------------------------------------------------------

Opa~\cite{opa, opa-pdf} is a statically typed unified web programming language whose main intent is in providing a framework suited for rapid and secure web development.
It allows the specification of a whole application in a single language, with automation of client/server calls.
This language has a clearly distinct goal from Loom's: it is mainly concerned with closing the gap between client, server, and database---focusing on aspects such as security and transparency.
As such, they do not offer, for instance, built-in support for reactive computations.
Yet, there are some shared concerns, namely in the unification of multiple languages into one: the Opa language provides \gls{xhtml} as a data-type, with special syntax support; similarly, it also provides \gls{css} as a data-type.

An example of a function in Opa that produces an \gls{xhtml} paragraph (adapted from~\cite{opa-book}) is as follows:
\begin{lstlisting}[language=loom]
function helloWorld() {
  style = css {color: white; background: blue; padding: 10px;}
  <p style={style}>Hello world</>
}
\end{lstlisting}
This example showcases Opa's syntax for \gls{xhtml} and \gls{css}, which are intended to look just like the real thing.

\gls{css} in Opa, however, is a data-structure that is either registered and served to the clients, becoming immutable, or is applied to \gls{xhtml} values through their \code{style} attribute, as shown in the example. This means that, aside from manipulating the \code{style} attributes of \gls{xhtml} elements, there is no way of creating dynamic presentations of an interface using \gls{css} as a data-structure.

% Elm --------------------------------------------------------------------------

Another language recently developed for the web is Elm~\cite{elm-paper, elm} --- an increasingly popular strict functional programming language that compiles to JavaScript.
Unlike Opa, and similarly to our language, Elm is mainly concerned with the development of client-side reactive applications.
Elm supports both \gls{html} and \gls{css} as data-types: values of such types are produced via functions offered by the language.

A fully working example of a tasks-manager application using Elm may be found in \cref{sec:simple-todo-app-elm}.
For a simpler example, the following code shows how a simple \gls{html} element representing a user profile (with the user's picture and name) may be built using Elm (adapted from~\cite{elm-html}):
\begin{lstlisting}[language=elm]
profile : User -> Html
profile user =
  div [class "profile"]
    [ img [src user.picture] []
    , span [style [("color", "red")]] [text user.name]
    ]
\end{lstlisting}
\code{div}, \code{img}, and \code{span} are functions that take a list of attributes and a list of \gls{html} elements and produce a new \gls{html} element. Similar functions exist for all the tags defined in the \gls{html}5 specification. All of these are actually helpers that use the \code{node} function: a function of type \code{String -> List Attribute -> List Html -> Html} (e.g. the \code{div} function is implemented as \code{div = node "div"}). \gls{html} attributes are themselves specified via functions; \code{style} is an example of such a function and takes a list of pairs of strings as argument.
This is the typical way of using \gls{css} directly in the Elm language: by being applied to an element's \code{style} attribute. Thanks to the language's reactive nature (explained in~\ref{subs:reactive-programming}) these styles may be dynamically updated on the event of some data-change: causing the presentation of a page to consequently update.

In contrast with Loom, Elm does not support the creation of style sheets that allow the full expressivity of \gls{css}: at least not first-class ones with support for reactivity.

% Ur/Web -----------------------------------------------------------------------

A third language that attempts to close the gap between multiple web technologies is Ur/Web~\cite{urweb}. Ur/Web is a unified web language, resembling Opa, that supports \gls{html} and \gls{sql} queries as first-class values, whilst supporting reactivity in a way similar to Elm. However, it seems to lack support for primitive \gls{css} values (again, reactivity in terms of presentation is still possible through \gls{html} element's \code{style} attribute).

% Frameworks -------------------------------------------------------------------

Angular~\cite{angular}, Meteor~\cite{meteor}, Ractive~\cite{ractive}, and React~\cite{react} are examples of frameworks built with the intent of bridging web technologies. What these frameworks have in common is that, instead of defining a new language that supports values such as \gls{html} or \gls{css}, they offer ways of better integrating already existent technologies.

Angular, for instance, extends \gls{html} with a set of directives that allows it to work as a template engine with reactive properties. In fact, a similarity between these frameworks is their concern with the definition of reactive structures (see \cref{subs:reactive-programming}).

%==============================================================================%
% Methods and Techniques                                                       %
%==============================================================================%

\section{Methods and Techniques}
\label{sec:methods}

\subsection{Gradual typing}
\label{subs:types-in-languages}

Gradual typing is a type system that supports both static and dynamic typing. It is a type system commonly applied on top of languages that were originally designed to be dynamic.
This is the case for JavaScript, on top of which extensions such as TypeScript~\cite{typescript} or Flow~\cite{flow}, with support for gradual typing, were defined.

As previously mentioned, this methodology for adding (optional) static typing to JavaScript comes with the advantage of not losing support for already available packages (e.g. obtained through \emph{npm}).
This is not the case for other languages that compile to JavaScript whilst forcing static typing, such as Opa or Elm.
In fact, in order to use external (JavaScript) packages in these languages, communication with the language must be performed through a provided \gls{api}.

Even though not yet implemented in Loom, this provides us with insight on how we may, in the future, benefit from type systems whilst keeping the principle of interoperability with JavaScript in mind.

%------------------------------------------------------------------------------%
% Reactive Programming                                                         %
%------------------------------------------------------------------------------%

\subsection{Reactive Programming}
\label{subs:reactive-programming}

A common problem encountered when writing client-side web applications is the monitoring of a certain value (e.g.\ the width of the browser's window; some text input in a form field; or the current time) with the intent of updating some other value whenever the first one changes.
Multiple patterns express this idea: polling and comparing, where the value is checked periodically and an update occurs when a change in the value is found; events, where an event is emitted every time the value changes and whoever is interested listens for such events in order to update; bindings, where values are represented by an object of some interface that allows the binding between such objects, thus forming a dependency graph that causes values to be automatically updated when a value they depend on changes.
A different approach, that has become increasingly popular, is reactive programming.

Reactive programming is a declarative style of programming: this means that the programmer specifies what is supposed to happen, rather than how. Modern spreadsheet programs provide an example of reactive programming: cells that contain formulae such as \code{=A1+B1} that depend on other cells are updated whenever the values of those other cells change.
This approach abstracts away the inner propagation of changes through the data flow.

Technologies such as Angular, Meteor, React, and Elm have popularised the usage of this technique on the web in order to create reactive interfaces.
However, whilst frameworks such as Angular, Meteor, or React often force a set of conventions such as models, views, controllers, or components to be adopted, languages like Elm or Ur/Web offer an approach based on \gls{frp}.

\gls{frp} is a declarative programming paradigm for working with time-varying values, known as signals~\cite{rt-frp}. A survey on the existent set of libraries that work with signals~\cite{reactive-interfaces} separates the usage of signals in two distinct categories: with combinators, or as signal expressions.
Combinator libraries, such as Flapjax~\cite{flapjax} or Elm, combine signals via functions, in order to produce new signals.
Signal expressions allow the definition of signals as arbitrary expressions of the host language.
These expression may depend on other signals, automatically updating whenever their value changes.
This is the approach followed by Loom; also worth noting is the fact that combinator functions may still be built on top of mutable values and signal expressions.

%------------------------------------------------------------------------------%
% Reactive Programming                                                         %
%------------------------------------------------------------------------------%

\subsection{Virtual DOM}
\label{subs:virtual-dom}

Regarding the usage of reactive techniques, an important observation must be made when updating interfaces in the context of the web: \gls{dom} updates are not cheap.
This is true for reactive structures specified as signals, or in any other ways.
This means that it is not wise to update a whole page whenever some value that the page depends on changes.
In fact, not only is it not cheap, it may compromise user experience (\textit{e.g.}\ if the user has some text box selected, an update to the \gls{dom} causes the mentioned text box to lose focus).
A popular approach to work around these issues lies in the usage of a virtual \gls{dom} structure: virtual \gls{dom}s allow for lightweight updates of the structure of a page by calculating the difference between two virtual structures, and patching the real \gls{dom} with only the elements that actually changed~\cite{elm-html, virtual-dom}.

Yet, why the need for a virtual \gls{dom}? The above problems could be solved using tradicional \gls{dom} manipulations; likely, even more efficiently. The real benefit of a virtual \gls{dom} is that the \gls{dom} manipulations are automated and abstracted whilst still being performant. Doing this sort of manual management requires keeping track of what has changed and what has not, in order to avoid updating large portions of a structure that do not require an update---a process typically error prone. This is why most of nowadays frameworks that provide some way of defining reactive structures in a declarative manner use the virtual \gls{dom} techniques: this includes Elm, React, Vue.js, and many others.

Loom's virtual \gls{dom} implementation, as previously mentioned, is built on top of Snabbdom---a lightweight low-level library which has been shown to be performant in practice (see \cref{sec:benchmarks}).
