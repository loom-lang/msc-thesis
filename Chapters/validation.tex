%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Validation                                                                 %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Validation}
\label{cha:validation}

In this chapter we enunciate some of the ways with which we validate Loom: we implement the TodoMVC application, making it possible to compare Loom against other technologies; and we explain how our underlying technologies have been shown performant in practice.

%==============================================================================%
% TodoMVC                                                                      %
%==============================================================================%

\section{TodoMVC}
\label{sec:todo-mvc}

The TodoMVC is a project that offers the same tasks-manager application implemented in a plethora of different languages and frameworks~\cite{todomvc}.
Thanks to this project, it is possible to effectively compare multiple technologies by having a common background application. \Cref{fig:todo-mvc} displays the expected interface of a TodoMVC application; our working version is available online at: \url{https://loom-lang.gitlab.io/loom-todomvc}.

In order to be able to compare Loom against other technologies, we implement the TodoMVC application with all required specifications.\footnote{The full list of specifications may be found at: \url{https://github.com/tastejs/todomvc/blob/master/app-spec.md}}
\Cref{sec:todo-mvc-code} contains the main code for our implementation of the application. It is worth noting that, as opposed to all other TodoMVC applications, all of the interface's presentation has been specified in Loom. The full source code may be found at: \url{https://gitlab.com/loom-lang/loom-todomvc}.

We leave to the reader the task of comparing our implementation with other implementations of the application. It is our belief that Loom's implementation is both concise and intuitive---though we might be biased.
Regarding performance, we found that our application performs as well as all others for a small number of tasks; however, at about two hundred tasks, it starts to lag: this is due to our functional approach to the implementation of the application without having decent underlying data structures to support it, as well as currently unoptimised \gls{css} support.
This can be solved in two ways: either rethink Loom with native support for immutable data-structures tailored for a more functional-style of programming; or change the implementation of the application so that it does not create a new \gls{html} value for each task every time the list of tasks is updated. In \cref{cha:conclusions} we see that this is an issue we intend to work on in the future. 

\begin{figure}
  \includegraphics[width=\textwidth]{todo-mvc.png}
  \caption{TodoMVC application (the displayed one was implemented in Loom---thought all of them look alike)}
  \label{fig:todo-mvc}
\end{figure}

%==============================================================================%
% Benchmarks                                                                   %
%==============================================================================%

\section{Benchmarks}
\label{sec:benchmarks}

In order to benchmark our implementation of Loom's patching algorithm, we extend an already existent ``JS Framework Benchmark''~\cite{js-benchmark}. This project contains implementations of multiple benchmarks for a plethora of different frameworks; as such, we provide an implementation of the benchmarks in Loom, for comparison.

The benchmarks in question consist on the creation of a large table with randomized entries and measuring the duration of various operations:
\begin{itemize}
  \item \textbf{Create rows:} duration of creating 1000 rows;
  \item \textbf{Replace all rows:} duration of updating all 1000 rows of the table (with 5 warm-up iterations);
  \item \textbf{Partial update:} duration of updating the text of every 10th row (with 5 warm-up iterations);
  \item \textbf{Select row:} duration of highlighting a row in response to a click on it (with 5 warm-up iterations);
  \item \textbf{Swap rows:} duration of swapping 2 rows on a 1000 rows' table (with 5 warm-up iterations);
  \item \textbf{Remove row:} duration of removing a row (with 5 warm-up iterations);
  \item \textbf{Create many rows:} duration of creating 10000 rows;
  \item \textbf{Append rows to large table:} duration of adding 1000 rows to a table of 10000 rows;
  \item \textbf{Clear rows:} duration of clearing the table filled with 10000 rows;
  \item \textbf{Startup time:} duration for loading and parsing the JavaScript code and rendering the page;
  \item \textbf{Ready memory:} memory usage after page load;
  \item \textbf{Run memory:} memory usage after adding 1000 rows.
\end{itemize}

For all benchmarks, the duration was measured with the rendering time included. \Cref{fig:benchmarks} shows a comparison between Loom's results and those of two of the most popular frameworks for the client-side development of web applications: React and Angular.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{benchmarks.png}
  \caption{Loom's benchmarks compared with React and Angular; each value is followed by: ± standard deviation (slowdown = duration / fastest)}
  \label{fig:benchmarks}
\end{figure}

As may be seen, Loom is currently not as optimised as the frameworks we are comparing it with; yet, the results also show that Loom performs very well at certain benchmarks: this is especially the case for the benchmarks where Loom takes advantage of its sub-tree patching (benchmarks 3 and 4).

We believe that Loom's poor results in benchmarks 5 and 6 are not a consequence of the employed patching algorithm, but rather of the fact that Loom requires an extra step to perform the actions: to swap two rows, Loom requires the creation of a new list that is a copy of the initial one with both rows swapped; to remove a row, Loom creates a new list that is a copy of the first one without the row to remove. Both Angular and React simply mutate the already existent list without creating a new one.

Benchmark 8, on the other hand, likely showcases the differences in terms of optimizations between Loom and the remaining libraries: which become apparent for very large structures.

With these results in mind, we can still say that Loom is currently performant enough in practice---though there is obviously still room for improvement. In all fairness, however, the presented benchmarks do not take into account the patching of \gls{css} values---which will likely cause yet another drop in performace; as we mention in the following chapter, measuring and minimising this impact will be left for future work.
