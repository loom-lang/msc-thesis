%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Programming Language                                                       %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Programming Language}
\label{cha:programming-language}

This chapter expands on the introductory one by presenting Loom in detail.
Throughout this chapter we expand on the language's main features and the principles and foundations on top of which they were built.
In particular, we explain the core of our programming language; detail the semantics and inspirations for Loom's reactive values (signals); go over Loom's first-class \gls{html} values and their semantics; introduce important design decisions regarding \gls{css} values; and finally, we explain how Loom's modules can be used to create fully working web applications.

\paragraph{Disclaimer} Before we move on with the description of the core language, we would like to make a harsh observation regarding Loom: as stated previously, Loom was specified and developed with the main intent of mixing client-side web technologies---the key concepts for integrating these technologies being the notion of signals, \gls{html}, and \gls{css} values as first-class citizens.
Most of the effort so far went into implementing such an integration in an efficient manner (more on this in \cref{cha:implementation}), effectively making this dissertation more of a \emph{practical} rather than \emph{theoretical} contribution.
We expand on the consequences of this in more detail in \cref{cha:conclusions}; however, in the context of this chapter, this means that Loom does not \emph{yet} have a defined type system or set-in-stone operational semantics---something we believe to be of great importance in a programming language.

%==============================================================================%
% Core Language                                                                %
%==============================================================================%

\section{Core Language}
\label{sec:core-language}

Loom's main goal as a programming language is in aiding the creation of client-centred web applications.
Targeting the web entails having JavaScript be output by Loom's compiler.
Whilst the target compilation language shouldn't have a great influence on Loom's design choices---with interoperability with JavaScript being one of our core design principles---the language became somewhat influenced by JavaScript.
A few other influences to Loom were Scala~\cite{scala} and CoffeeScript~\cite{coffeescript}.

Loom is thus a multi-paradigm programming language with support for imperative and functional programming styles.
\Cref{fig:loom-syntax} showcases a simplified version of Loom's concrete syntax where separator symbols, syntactic sugar, and certain non-relevant constructs are omitted.\footnote{The most notable syntactic sugar omitted from the grammar---used throughout the document in examples---regards the usage of block expressions after many of Loom's constructs.
Such block expressions may have the \code{do} keyword omitted: which has the side effect of forcing extra parenthesis when an object is instead desired.
Note that the same is done for the \code{css} keyword when nesting \gls{css} values.}

% Grammar ----------------------------------------------------------------------

\begin{figure}
\begin{grammar}
<prog>
  ::= <topStat>"*"
    \ruledesc{Program (sequence of top-level statements)}

<topStat>
  ::= `import' ID `from' STRING
    \ruledesc{Import default}
  \alt `import' `\{' "(" ID "(" `as' ID ")?" ")*" `\}' `from' STRING
    \ruledesc{Named import}
  \alt `export' `default' <exp>
    \ruledesc{Export default}
  \alt `export' <decl>
    \ruledesc{Export declaration}
  \alt <stat>
    \ruledesc{Statement}

<stat>
  ::= <decl>
    \ruledesc{Declaration}
  \alt <exp>
    \ruledesc{Expression statement}

<decl>
  ::= <varDecl>
    \ruledesc{Variable declaration}
  \alt `function' ID `(' ID"*" `)' <exp>
    \ruledesc{Function declaration}

<varDecl>
  ::= "(" `var' "|" `const' ")" "(" ID `=' <exp> ")+"
    \ruledesc{Variable declaration}

<exp>
  ::= `if' `(' <exp> `)' <exp> "(" `else' <exp> ")?"
    \ruledesc{Conditional expression}
  \alt `for' `(' <varDecl> `in' <exp> `)' <exp>
    \ruledesc{Array comprehension}
  \alt `for' `(' <varDecl> `of' <exp> `)' <exp>
    \ruledesc{Object comprehension}
  \alt `while' `(' <exp> `)' <exp>
    \ruledesc{While expression}
  \alt `try' <exp> "(" `catch' `(' ID"?" `)' <exp> ")?" "(" `finally' <exp> ")?"
    \ruledesc{Try expression}
  \alt `throw' <exp>
    \ruledesc{Throw expression}
  \alt `do' `\{' <stat>"*" `\}'
    \ruledesc{Block expression}
  \alt <assig> `=' <exp>
    \ruledesc{Assignment}
  \alt $\ominus$ <exp> "|" <exp> $\oplus$ <exp>
    \ruledesc{Unary/binary expression}
  \alt `[' "(" <exp> "|" `...' <exp> ")*" `]'
    \ruledesc{Array value}
  \alt `\{' "(" ID `:' <exp> "|" `...' <exp> ")*" `\}'
    \ruledesc{Object value}
  \alt <exp> `.' ID "|" <exp> `[' <exp> `]'
    \ruledesc{Selection}
  \alt `(' ID"*" `)' `=>' <exp>
    \ruledesc{Function expression}
  \alt <exp> `(' <exp>"*" `)'
    \ruledesc{Function application}
  \alt `mut' <exp>
    \ruledesc{Mutable signal}
  \alt `sig' <exp>
    \ruledesc{Signal expression}
  \alt `*' <exp>
    \ruledesc{Signal dereferenciation}
  \alt `<' ID "(" `#' ID ")?" "(" `.' ID ")*" <exp>"?" "(" `/>' "|" `>' <exp> ")"
    \ruledesc{HTML value}
  \alt `css' `\{' "(" ID `:' <exp> "|" `...' <exp> "|" `|' <sel>"*" `|' <exp> ")*" `\}'
    \ruledesc{CSS value}
  \alt ID
    \ruledesc{Identifier}
  \alt STRING "|" NUMBER "|" `true' "|" `false' "|" `null' "|" `undefined'
    \ruledesc{Literal}

<assig>
  ::= ID "|" <exp> `.' ID "|" <exp> `[' <exp> `]' "|" `*' <exp>
    \ruledesc{Assignable}

<sel>
  ::= <simpSel> "(" "(" \textvisiblespace{} "|" `>' "|" `+' ")" <simpSel> ")*"
    \ruledesc{CSS selector}

<simpSel>
  ::= "(" `*' "|" `&' "|" ID ")?" "(" `#' ID "|" `.' ID "|" `:' ID "(" `(' ID `)' ")?" ")*"
    \ruledesc{Simple CSS selector}
\end{grammar}
\caption{Simplified syntax of Loom in EBNF notation}
\label{fig:loom-syntax}
\end{figure}

Although most of the keywords in Loom match their JavaScript counterparts, as we may observe---in similarity with languages such as Scala or CoffeeScript---Loom attempts to allow, when possible, constructs to be used as expressions.
This brings us the benefit of being able to easily compose complex expressions---often desired when defining the structure of a user interface.
Because the operational semantics of some of these expressions may not be obvious, we'll informally go over them:
\begin{itemize}
% If then else -----------------------------------------------------------------
\item \code{if (e1) e2 else e3} is a typical \emph{if then else} expression that, depending on the result of evaluating \code{e1}, evaluates to the evaluation of \code{e2} or \code{e3}.
Note that \code{if (e1) e2} is a valid Loom expression and is equivalent to \code{if (e1) e2 else undefined};

% Loops ------------------------------------------------------------------------
\item All loops defined in Loom evaluate to an array where each element is the result of evaluating the loop's body in the respective step of the iteration.
As an example, \code{for (var i in [1, 2, 3]) i * i} evaluates to \code{[1, 4, 9]};

% Try/catch --------------------------------------------------------------------
\item \code{try e1 catch(err) e2 finally e3} is an expression that evaluates to the result of evaluating \code{e1} or \code{e2} depending on whether an error occurs during the evaluation of \code{e1}; the \code{finally} portion of the expression is only meaningful for side-effects.
Similarly to the \emph{if then else} expression, \code{try e1} is a valid Loom expression that is equivalent to \code{try e1 catch(err) undefined}.
As an example, \code{try 1 catch(err) err} evaluates to \code{1} and \code{try throw "Error" catch(err) err} evaluates to the string \code{\"Error"};

% Block ------------------------------------------------------------------------
\item A block is an expression in Loom that evaluates to the result of evaluating its last statement after evaluating all previous statements. Note that variable declarations evaluate to \code{undefined}; function declarations evaluate to a value representing the function itself; and expression statements evaluate to the result of evaluating the expression. This means that \code{do \{var x = 1; x;\}} evaluates to \code{1};

% Array/object spread ----------------------------------------------------------
\item The \code{...} operator that may be used in arrays and objects is called a \emph{spread} operator and has the same semantics as the JavaScript counterpart\footnote{In fact, spread object properties are, at the time of this writing, a stage 3 proposal for ECMAScript, see: \url{https://github.com/sebmarkbage/ecmascript-rest-spread}.}: it expands an expression where multiple elements (for arrays) or multiple properties (for objects) are expected. As an example, \code{[1, ...[2, 3], 4]} evaluates to the array \code{[1, 2, 3, 4]}; \code{\{a: 1, ...\{b: 2, c: 3\}, c: 4\}} evaluates to the object \code{\{a: 1, b: 2, c: 4\}}. This operator is useful as it enables a functional style of updating arrays/objects.
\end{itemize}
The semantics for other relevant expressions such as signal, \gls{html}, and \gls{css} values are explained in subsequent sections.

%==============================================================================%
% First-Class Reactivity                                                       %
%==============================================================================%

\section{First-Class Reactivity}
\label{sec:first-class-reactivity}

As previously mentioned in \cref{cha:introduction}, Loom supports signals as first-class values; but what are signals? Signals---initially introduced as behaviours---are a term used in the context of \gls{frp} to capture the temporal aspect of value mutability; they thus represent values that change over time (we go over the topic of reactive programming in more detail in \cref{subs:reactive-programming}).
Loom's signal values were heavily influenced by those of Scala.React~\cite{scala-react} and Scala.Rx~\cite{scala-rx} and may be defined as one of two types: mutables (similar to \code{Var}s in Scala.React and React.Rx) and signal expressions (conceptually equivalent to \code{Signal}s in Scala.React and \code{Rx} in React.Rx). Let us look at each type in more detail:

% Signal -----------------------------------------------------------------------

\paragraph{Signal} A signal in Loom may be seen as the common interface for both mutable values and signal expressions. All signals share the same core functionality: signals hold a \emph{current value} (which in Loom may be accessed through the \code{*e} expression); signals may be \emph{subscribed to} by other signals; and signals may be \emph{observed}. Note that \emph{subscribing} to a signal and \emph{observing} a signal are two conceptually distinct actions: \emph{subscriptions} define a dependency graph between signals---shortly, we will see that signal expressions implicitly subscribe to other signals; \emph{observing} a signal implies running an arbitrary function whenever a signal's value changes---which may produce side-effects.

% Mutable ----------------------------------------------------------------------

\paragraph{Mutable} A \code{mut e} expression in Loom creates a mutable value---a value that may be seen as the \emph{source} of a reactive computation.
Changing the currently held value of a mutable may be done in Loom with a \code{*e1 = e2} expression (similarly to the \code{e1() = e2} expression in Scala.React and Scala.Rx).
As previously mentioned, signals may subscribe to other signals; a mutable value is always a \emph{source} in the dependency graph that such subscriptions represent: this means that an update to a mutable value conceptually causes all dependent signals to update.\footnote{Note that we say \emph{conceptually} because this behaviour is, in fact, implementation dependent (push vs.\ pull based propagation; see \cref{sec:reactive-values})---it should, however, be transparent to the user.}

% Signal expression ------------------------------------------------------------

\paragraph{Signal expression} Loom supports the composition of signals via signal expressions: using a \code{sig e} syntax.
A signal expression, during the evaluation of its expression, implicitly and dynamically subscribes to signals whose values are accessed inside of its static scope.
As an example, consider the declarations \code{var x = mut 1} and \code{var y = sig *x + 1}: \code{y} is a signal expression whose value is \code{2} (\code{*y} evaluates to \code{2}) and whose expression accesses the value of \code{x}---becoming dependent of it.
This means that an update to \code{x} such as \code{*x = 5} causes \code{*y} to evaluate to \code{6}.
As another example, admit the same \code{x} and consider the function \code{var f = n => *x + n}: a signal such as \code{sig f(3)} will not subscribe to \code{x}---because \code{x} is not accessed in the signal's static scope.
This behaviour is important in order to prevent the risk of adding unwanted dependencies to the signal expression.
For a third example, and considering the same \code{x} definition, the signal \code{sig if (true) 5 else *x} does not depend on \code{x}---the signal never accesses the value of \code{x} during the evaluation of its expression: showcasing the dynamic detection of signal dependencies.
Signal expressions are thus a powerful mechanism that allows a declarative composition of dependencies by taking advantage of all language constructs.

%==============================================================================%
% Dynamic Interfaces With First-Class HTML                                     %
%==============================================================================%

\section{Dynamic Interfaces With First-Class HTML}
\label{sec:first-class-html}

Recall Loom's syntax for defining \gls{html} values---which may more naturally be specified as:
\begin{grammar}
<html>
  ::= `<' ID "(" `#' ID ")?" "(" `.' ID ")*" <exp>"?" `/>'
    \ruledesc{HTML value with no children}
  \alt `<' ID "(" `#' ID ")?" "(" `.' ID ")*" <exp>"?" `>' <exp>
    \ruledesc{HTML value with children}
\end{grammar}
The expression \code{<t\#i.c.d e1> e2} represents an \gls{html} element with tag \code{t}, identifier \code{i}, and classes \code{c} and \code{d}; the result of evaluating \code{e1} represents the attributes of the element and of evaluating \code{e2} its children.
As an example, consider:
\begin{lstlisting}
<button#foo.bar.baz {type: "submit"}> [
  "Press me"
]
\end{lstlisting}
This \gls{html} value represents the following \gls{html} element:
\begin{lstlisting}
<button id="foo" class="bar baz" type="submit">Press me</button>
\end{lstlisting}
Note that defining an \gls{html} value with no children such as \code{<div/>} is equivalent, in Loom, to defining it with an empty array of children: \code{<div> []}.

Loom's semantics regarding \gls{html} values are, as far as we are aware, unique in relation to other technologies due to the \emph{kind} of values its inner expressions are allowed to evaluate to. In particular, \gls{html} allows signals to be used virtually anywhere; \cref{fig:html-schema} showcases a possible simplified schema for the representation of an \gls{html} value and the \emph{kind} of values it expects in each of its fields.\footnote{Note that, as previously mentioned, Loom does not yet have a defined type system---being as dynamic as its target compilation language: JavaScript.
However, we may see how future work involving the development of such a type system may force Loom's \gls{html} values to follow this schema.}
\begin{figure}
\begin{grammar}
<HTML>
  ::= \{tag: string, id: string"?", classes: List"["string"]", attrs: <Attrs>"?", childr: <Childr>\}

<Attrs>
  ::= Map"["string"," <Attr>"]" "|" Signal"["Map"["string"," <Attr>"]""]"

<Attr>
  ::= string "|" boolean "|" Function "|" Signal"["string "|" boolean "|" Function"]"

<Childr>
  ::= List"["<Child>"]" "|" Signal"["List"["<Child>"]""]"

<Child>
  ::= "("<HTML> "|" string")?" "|" Signal"[""("<HTML> "|" string")?""]"
\end{grammar}
\caption{Possible simplified representation of the schema of an HTML value; the ``?'' notation indicates that the value may be \emph{undefined}}
\label{fig:html-schema}
\end{figure}

Though we have seen how \gls{html} values may be \emph{defined} in Loom---as well as what values they support---how are they effectively \emph{rendered} to the screen?
This is explained in detail in \cref{sec:html-values} and briefly exposed in \cref{sec:modules-definition}; for now, let us assume that \gls{html} values are \emph{applied} to the \gls{dom}---making the \gls{dom}'s structure match that of the applied \gls{html} value.
With this in mind, we can now dive into what it means for a signal to be part of an \gls{html} value:

Conceptually, as signals are seen as values that change over time, signals used inside of \gls{html} represent parts of the structure that change over time.
This means that when a signal's value is updated, the part of the structure containing such signal is \emph{updated}; if such structure is in fact being \emph{rendered} on the screen, then the \gls{dom} consequently updates to match the signal's most recent value.

This updating of the \gls{dom} to make it match the structure of a signal's value effectively causes the \emph{state} of the \gls{dom} to become consistent with that of the signal. Yet, are they \emph{always} consistent? What about user interaction?
In fact, user interaction (\textit{e.g.}\ a user writing in a text box) produces changes to the \emph{state} of the \gls{dom}---causing the interface's structure to get \emph{out of sync} in respect to the one specified with Loom's \gls{html} values.
For this reason, Loom allows certain attributes of an element---those that may change due to user interaction (\textit{e.g.}\ the \code{value} attribute of an \code{<input/>} element)---to be \emph{aware} of user input: this can be done by setting the value of such attributes as a mutable signal---causing the signal to update whenever user input occurs. As an example, consider:
\begin{lstlisting}
var text = mut "Initial value"
var elem = <input {value: text}/>
\end{lstlisting}
If \code{elem} is rendered on the screen, user interaction with the text box will propagate to the \code{text} mutable signal: conceptually keeping the \gls{dom} and the \gls{html} value's state consistent at all times. This behaviour is typically defined as a \emph{two-way binding} between the \gls{dom} and its \emph{virtual} representation---in Loom's case, the \gls{html} value.

%==============================================================================%
% CSS as a First-Class Citizen                                                 %
%==============================================================================%

\section{CSS as a First-Class Citizen}
\label{sec:first-class-css}

\gls{css} style sheets are, in Loom, first-class values that may be defined using the following (previously mentioned) syntax:
\begin{grammar}
<css>
  ::= `css' `\{' "(" ID `:' <exp> "|" `...' <exp> "|" `|' <sel>"*" `|' <exp> ")*" `\}'
    \ruledesc{CSS value}

<sel>
  ::= <simpSel> "(" "(" \textvisiblespace{} "|" `>' "|" `+' ")" <simpSel> ")*"
    \ruledesc{CSS selector}

<simpSel>
  ::= "(" `*' "|" `&' "|" ID ")?" "(" `#' ID "|" `.' ID "|" `:' ID "(" `(' ID `)' ")?" ")*"
    \ruledesc{Simple CSS selector}
\end{grammar}
Note that, for the sake of simplicity, the above grammar does not support all existent \gls{css} selectors---supporting only a subset of them; however, Loom's actual grammar does.

As may be inferred from the grammar, Loom supports \emph{nesting} of \gls{css} rules; with semantics similar to those provided by \gls{css} preprocessors (this is explained in more detail in \cref{subs:css-preprocessors}). As an example, consider:
\begin{lstlisting}
css {
  color: "blue"
  |a > i, a:hover > b| {
    color: "green"
  }
  |&:focus| {
    color: "purple"
    |div| {
      color: "yellow"
    }
  }
}
\end{lstlisting}
This \gls{css} value represents the following \gls{css} style sheet, where \code{\[scoped\]} conceptually represents the element the \gls{css} value was \emph{applied} to (more on this below):
\begin{lstlisting}[language=css]
[scoped] {
  color: blue;
}
[scoped] a > i, [scoped] a:hover > b {
  color: green;
}
[scoped]:focus {
  color: purple;
}
[scoped]:focus div {
  color: yellow;
}
\end{lstlisting}

Another feature supported by Loom's \gls{css} values is the ability to \emph{import} other \gls{css} values: the \code{\...e} syntax, inspired by JavaScript's spread operator, expects \code{e} to be a \gls{css} value and \emph{imports} all of its properties and rules.
This makes it possible to easily share common \gls{css} properties by defining them elsewhere and importing them where needed. As a simple example, consider:
\begin{lstlisting}
var red = css {
  backgroundColor: "red"
  border: "1px solid black"
}
var styleSheet = css {
  display: "block"
  ...red // Import properties
  border: "2px solid blue" // Override the property
}
\end{lstlisting}
This example showcases an important property of \gls{css}: the order in which properties are defined matters---which allows for properties to be overridden.

Now that we have seen how \gls{css} values are defined in Loom, how can they actually be used? In contrast with typical \gls{css} style sheets, Loom's style sheets are \emph{applied} (and \emph{scoped}) to elements---thus avoiding the \emph{leakage} of certain styles to unwanted parts of an interface (explained in more detail in \cref{sec:css-values}).
\gls{css} values may be applied to a given \gls{html} value by using their \code{css} attribute. The above \code{styleSheet} may thus be applied to some \code{<div/>} \gls{html} value with: \code{<div \{css: styleSheet\}/>}.

The final---and arguably most important---feature of Loom's style sheets lies in their ability to, in conformity with \gls{html} values, support reactivity: allowing for the definition of dynamic data-dependent presentations.
\Cref{fig:css-schema} displays a simplified schema for the representation of a \gls{css} value, highlighting the \emph{kind} of values it expects in each of its fields.
\begin{figure}
\begin{grammar}
<CSS>
  ::= \{props: SortedMap"["string"," <Val>"]", rules: SortedMap"["List"["<Selector>"]", <Body>"]"\}

<Val>
  ::= number "|" string "|" Signal"["number "|" string"]"

<Body>
  ::= <CSS> "|" Signal"["<CSS>"]"
\end{grammar}
\caption{Possible representation of the schema of a CSS value; selectors' schema is not shown as it could get too complex---each type of selector would have its own schema; Sorted maps are used to represent insertion order}
\label{fig:css-schema}
\end{figure}
As shown, signals may be used as values of \gls{css} properties and as the body for \gls{css} rules---their semantics being as expected: they represent the parts of a style sheet that may change over time. \gls{html} values rendered on the \gls{dom} that have \gls{css} values \emph{applied} to them will thus be able to have dynamic presentations, so long as their \gls{css} contains such signals.

%==============================================================================%
% Modules: Writing Only in Loom                                                %
%==============================================================================%

\section{Modules: Creating a Web Application in a Single Language}
\label{sec:modules-definition}

A \emph{recent} specification for JavaScript---\gls{es6}---introduced the notion of \emph{modules} in the language; with the principle of interoperability in mind, Loom's module system was designed with the same semantics as those defined in the specification. As such, Loom supports all of the available \code{import} and \code{export} syntactical constructs in \gls{es6}, even though \cref{fig:loom-syntax} only showcases the most useful ones.

Briefly, a file in Loom (also in \gls{es6}) is seen as a module; both \emph{names} and a \emph{default value} may be imported or exported by modules.
As an example, consider a file containing the following:
\begin{lstlisting}
import {foo, bar as baz} from "some-module"
import defaultValue from "other-module"
export var aNumber = 10
export default "some string"
\end{lstlisting}
This file imports \code{foo}---bound to local name \code{foo}---and \code{bar}---bound to local name \code{baz}---from \code{\"some-module"}; as well as the default value---assigned to local name \code{defaultValue}---from \code{\"other-module"}.
The file further exposes the \code{aNumber} name and a default value containing the string \code{\"some string"}.

In order for a web application to be launched, an \gls{html} file has to be rendered by the browser: this typically forces users of a language or framework to create a ``main'' \gls{html} file---commonly named \code{index.html}---to \emph{launch} the application.
Loom intends to allow the full specification of a web application in a single language; as such, we extend the semantics of our module system to allow the generation of these ``main'' \gls{html} files.

This is done in Loom by exporting a \emph{default} \gls{html} value with an \code{html} tag. As an example, the following listing should generate an \gls{html} file that may be launched:
\begin{lstlisting}
var x = 10
export default <html> [
  <head> [
    <title> "Example"
  ]
  <body> [
    <span> x
  ]
]
\end{lstlisting}

\paragraph{Note} Although intended as a core feature of the language, the current implementation of the Loom compiler does not \emph{yet} generate \gls{html} files as a result of exporting \gls{html} values. This is due to Loom's mentioned lack of a type system: without which it is not possible to understand whether an exported value is indeed of type \gls{html} value with an \code{html} tag.
