%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Implementation Details                                                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Implementation Details}
\label{cha:implementation}

Now that we have defined the language, it is time to detail how it was, in practice, implemented.
This chapter showcases the implementation of Loom's compiler: we provide a JavaScript implementation of the compiler---making it possible to use it from within a web browser: Loom's playground takes advantage of this.

The compiler's source code may be found at: \url{https://gitlab.com/loom-lang/loom}.

Throughout the chapter, we detail how the main concepts of our language were implemented and the advantages of our approach over alternatives---as well as some of their limitations.
In particular, we briefly introduce the architecture of our implementation; we expose our implementation of signals; we detail our implementation of \gls{html} values---including how to efficiently integrate them with signals: allowing for performant \gls{dom} mutations; and finally, we explain our implementation of \gls{css} values---including how they, in similarity with \gls{html}, integrate with signals.

%==============================================================================%
% Architecture                                                                 %
%==============================================================================%

\section{Architecture}
\label{sec:architecture}

In the general case, Loom's compiler is a tool that, when receiving a Loom program as input, transforms it into a JavaScript program.
The compiler was implemented taking into consideration proper compiler design principles; \cref{fig:architecture} displays the typical compilation process for a Loom program.

\begin{figure}
  \includegraphics[width=\textwidth]{architecture.png}
  \caption{Loom's typical compilation process}
  \label{fig:architecture}
\end{figure}

Such process may be described as follows: given a Loom program, we parse it into its \gls{ast} representation; we then compile it to an \gls{ast} representation of JavaScript; this \gls{ast} representation is then possibly transformed into an equivalent JavaScript \gls{ast}; JavaScript source code is finally generated from the latest \gls{ast}.

Note that, as previously mentioned, the compiler does not yet produce \gls{html} files from exported \gls{html} values; we expect this behaviour to be implemented after defining and implementing a type system for the language.

The two final steps of the compilation are handled by Babel~\cite{babel}. Babel is, in simple terms, a compiler from JavaScript to JavaScript: it is able to ``understand'' the most recent JavaScript syntax and compiles it to equivalent constructs using widely supported syntax.
Babel further exposes its \gls{ast} representation for JavaScript.
From Loom's perspective, Babel is a useful library for two main reasons: we do not have to be concerned with the generation of actual JavaScript code---which is handled by Babel---only having to compile to Babel's \gls{ast} representation of JavaScript; and we may compile Loom's constructs against the latest JavaScript specification without having to worry about platform support for it. In fact, because Babel's transformations work with the notion of plugins, users of Loom---by defining which plugins to use---may specify their programs to be compatible with whichever version of JavaScript they desire.

Regarding the parsing step of the compilation, the Loom compiler uses PEG.js~\cite{peg-js} for the definition of Loom's grammar.
PEG.js is a parser generator that, given a \gls{peg}, outputs a JavaScript parser.
We define our grammar as a \gls{peg} instead of a more common \gls{cfg}---which, for example, Jison~\cite{jison} understands---because some constructs in Loom would be difficult to express in a \gls{cfg} without being ambiguous: \gls{peg}s just make them easier to specify.

In order to compile complex values such as signals, \gls{html}, or \gls{css}---whilst keeping the semantics defined in \cref{cha:programming-language}---Loom's compiler uses \emph{modules}: JavaScript files that we import from the generated JavaScript.
These modules contain the logic required to implement Loom's semantics regarding these complex values.

%==============================================================================%
% Using Loom's Compiler                                                        %
%==============================================================================%

\section{Using Loom's Compiler}
\label{sec:compiler-usage}

Before moving on with the details on how most of the language's concepts are implemented, let us first show how Loom's compiler may be used.

As previously mentioned, the source code for Loom's compiler may be found at \url{https://gitlab.com/loom-lang/loom}; it is also available for installation in npm via the \code{loom-lang} package (installed with \code{npm install -g loom-lang}).

Loom provides two ways of being interacted with: through a \gls{cli} and through a JavaScript \gls{api}.

The JavaScript \gls{api} mainly exposes two functions: \code{parse} and \code{compile}. The first one receives a JavaScript string representing a Loom program and outputs the \gls{ast} representation of said program; the second function takes a Loom program's \gls{ast} as input as well as a set of options (\textit{e.g.}\ Babel plugins to use; whether to output the JavaScript \gls{ast}) and outputs the compiled JavaScript.
Loom's playground (see \cref{sec:loom-playground}) takes advantage of this \gls{api} in order to compile Loom programs in a browser, on the fly.

The \gls{cli} uses the JavaScript \gls{api} to allow the compilation of files. At the time of this writing, Loom's \gls{cli} makes available the following options:

\begin{lstlisting}[language=tex, numbers=none, frame=none]
-c, --compile    output compiled code                                 [boolean]
-e, --eval       evaluate compiled code                               [boolean]
-h, --help       display help message                                 [boolean]
-i, --input      file used as input instead of STDIN                   [string]
-o, --output     file used for output instead of STDOUT                [string]
-p, --parse      output parsed Loom AST                               [boolean]
-v, --version    display version number                               [boolean]
--cli            pass string from the command line as input            [string]
--js-ast         output compiled JavaScript AST (Babylon)             [boolean]
--repl           run an interactive Loom REPL                         [boolean]
--babel-presets  specify Babel presets to use in compilation            [array]
--babel-plugins  specify Babel plugins to use in compilation            [array]
\end{lstlisting}

As shown, Loom's compiler may be ran in interactive \gls{repl} mode using the \code{--repl} flag.\footnote{Running Loom's compiler with no arguments also starts the interactive \gls{repl} mode.} This was implemented using Node.js' \gls{repl} module: each line of input is compiled as a Loom program; the generated JavaScript code is immediately evaluated and its value printed on the screen.

%==============================================================================%
% Implementing Signals                                                         %
%==============================================================================%

\section{Implementing Signals}
\label{sec:reactive-values}

To honour the semantics specified in \cref{sec:first-class-reactivity}, we provide signals as modules for Loom: this means that the generated JavaScript obtained from compiling a Loom program using signals will import the signals' module.
In fact, we provide two distinct modules: one exporting a JavaScript class that represents mutable values; the other representing signal expressions. As an example, consider the following program in Loom:
\begin{lstlisting}
var x = mut 10
var y = sig *x + 5
*x = 20
*y
\end{lstlisting}
The Loom compiler compiles the above program to the following (\emph{prettified}) JavaScript:
\begin{lstlisting}[language=js]
import Mut from "loom-lang/mutable";
import Sig from "loom-lang/signal-expression";
let x = new Mut(10);
let y = new Sig(sig => {
  return x.subscribe(sig) + 5;
});
x.setValue(20);
y.getValue();
\end{lstlisting}
The above mentioned modules are the ones imported in the JavaScript listing: \code{Mut} is a JavaScript class that represents a mutable value in Loom; \code{Sig} a JavaScript class representing signal expressions.

To understand the remaining generated code, let us first reveal some inner concepts regarding the signals' implementation. Recall the signals' core functionality: signals hold a \emph{current value}; signals may be \emph{subscribed to} by other signals; and signals may be \emph{observed}. Furthermore: mutable values may be \emph{assigned new values} over time; and a signal expression---during the evaluation of its expression---implicitly and dynamically subscribes to signals whose values are accessed inside its static scope.
With this, we define the following pseudo-interfaces (with \emph{pseudo-types}) for signals:
\begin{lstlisting}[language=js]
Signal[T] {
  subscribe: Signal[?] => T,
  observe: (T => Void) => {stop: () => Void},
  getValue: () => T
}

Mutable[T] <: Signal[T] {
  constructor: T => Mutable[T],
  setValue: T => T
}

SignalExpression[T] <: Signal[T] {
  constructor: (SignalExpression[T] => T) => SignalExpression[T]
}
\end{lstlisting}
Notice how signals may \emph{subscribe} to other signals: the \code{subscribe} method on a signal receives the signal performing the subscription as an argument and returns the current value of the signal being subscribed.

A signal expression is constructed by providing a function whose body represents the actual \emph{expression} of the signal and which receives the signal expression itself as an argument, \textit{i.e.}\ the constructor calls the received function with \code{this} as argument.

Now we can understand the example showcased above for the compilation of \code{y}: the signal's expression, when executed, subscribes to \code{x}---who becomes responsible for notifying \code{y} whenever it changes.

However, we are yet to answer an important question regarding our implementation (previously raised in \cref{sec:first-class-reactivity}): when \code{x} changes, \code{y} is notified; yet, when is \code{y}'s expression reevaluated?
Two typical approaches follow from this question: a \emph{push}-based approach would update \code{y} immediately; a \emph{pull}-based approach updates \code{y} only when its value is effectively requested (in the example, \code{y} would only be reevaluated when calling \code{y.getValue()}).
Both solutions have advantages and disadvantages (they both avoid/require needless recomputations in different situations); Loom opted for the \emph{pull}-based approach---the reason being that it allows us, as we will see in the following section, to define reactive \gls{html} values that are only updated when actually being rendered in the \gls{dom}.

For the sake of completeness, there is yet another caveat in Loom's implementation of signals. Consider the following example with a signal expression \code{z} whose value is a function that returns the value of a signal it receives:
\begin{lstlisting}
var w = mut 3
var z = sig a => *a
*z(w)
\end{lstlisting}
Loom will compile the above program to (omitting the imports):
\begin{lstlisting}[language=js]
let w = new Mut(3);
let z = new Sig(sig => {
  return a => {
    return a.subscribe(sig);
  };
});
z.getValue()(w);
\end{lstlisting}
If implemented naïvely, this example would cause \code{z} to subscribe to arbitrary signals whenever the function obtained from evaluating its expression is called with a new signal: we call this \emph{leaking}.
Yet again, recall our defined semantics for signal expressions: a signal expression---\emph{during the evaluation of its expression}---implicitly and dynamically subscribes to signals whose values are accessed inside its static scope.
This means that signal expressions should only ever subscribe to signals \emph{whilst} their expressions are evaluating. This is why our implementation keeps track of whether the signal expression is currently being evaluated; if it is not, calling the \code{subscribe} method is innocuous, behaving as a \code{getValue} call.

%==============================================================================%
% HTML Values and the Virtual DOM                                              %
%==============================================================================%

\section{HTML Values and the Virtual DOM}
\label{sec:html-values}

In similarity with signals, Loom compiles \gls{html} values that follow the semantics specified in \cref{sec:first-class-html} with the aid of modules. As an example, consider the following Loom program:
\begin{lstlisting}
var x = mut "Hello world!"
var elem = <div#foo> [
  <span.bar {title: "baz"}> [x]
  <img {src: "http://url.com/image.jpg"}/>
]
elem.renderTo(document.getElementById("foo"))
*x = "Goodbye!"
\end{lstlisting}
Which Loom's compiler transforms into:
\begin{lstlisting}[language=js]
import Mut from "loom-lang/mutable";
import VElem from "loom-lang/virtual-element";
let x = new Mut("Hello world!");
let elem = new VElem("div", "foo", [], {}, [
  new VElem("span", null, ["bar"], {title: "baz"}, [x]),
  new VElem("img", null, [], {src: "http://url.com/image.jpg"}, [])
]);
elem.renderTo(document.getElementById("foo"));
x.setValue("Goodbye!");
\end{lstlisting}
As shown, Loom's \gls{html} values are compiled into \code{VElem}s---virtual elements---that take the tag name, identifier, list of classes, attributes, and children of the respective \gls{html} value as constructing arguments. We will go over virtual elements and the meaning of \emph{virtual} shortly; let us first explain the \code{renderTo} portion of the code:

We have previously left in the open how exactly \gls{html} values \emph{render} on the screen. Rendering an element on the screen is the premise behind \code{renderTo} which---given an actual \gls{dom} node\footnote{It might appear like we are using the words \emph{element} and \emph{node} interchangeably; although not very important to the explanation, note that a \gls{dom} element (\textit{e.g.}\ a \code[html]{<div>}) is a \emph{type} of \gls{dom} node; other types include \gls{dom} text nodes and comment nodes.}---renders the virtual element \emph{on top} of the given node. Assuming that the previous example was compiled into an ``example.js'' file and that we have the following \gls{html} document:

\begin{lstlisting}[language=html]
<!DOCTYPE html>
<html>
  <head></head>
  <body>
    <div id="foo"></div>
    <script src="./example.js"></script>
  </body>
</html>
\end{lstlisting}

Then, the \code[html]{<div id="foo">} element rendered from the \gls{html} document would be mutated to match the structure that \code{elem} represents: in this case, two new \gls{dom} elements---a \code[html]{<span>} and an \code[html]{<img>}---would be created and appended to the \code[html]{<div>} with all respective attributes and children. Note that, although the \code[html]{<span>} contains a signal as a child, it would render to: \code[html]{<span class="bar" title="baz">Hello world!</span>}, \textit{i.e.}\ the signal's value is accessed when rendering the element.

The main questions that arise when implementing \gls{html} values---bound by the previously introduced semantics---involve how to update a structure. In the example, \code{x} is a signal whose value mutates at some point; how can changes to a signal affect what is displayed? And how can it be done efficiently?

Before answering these questions, it is worth pointing out a few facts: in practice, a signal inside an \gls{html} value may evaluate to an arbitrarily complex structure (as opposed to the simple text in this example); \gls{dom} mutations are expensive, rerendering the whole interface or even a given \emph{section} of it whenever a value that is part of such a \emph{section} changes is impractical (see \cref{subs:virtual-dom}): \gls{dom} manipulations should thus be kept to a minimum.

This is a problem that many other technologies have solved by introducing the notion of a \emph{virtual} \gls{dom}: a structure that is a representation of the actual \gls{dom} and that allows efficient manipulation of its components.
Conceptually, when an update is made to the virtual \gls{dom}, it is \emph{diffed} against its older representation: updating the actual \gls{dom} only where this \emph{diffing} detects changes.
As one might infer, Loom's \code{VElem}s represent virtual elements in a virtual \gls{dom} representation.

Internally, Loom uses a library that goes by the name of Snabbdom~\cite{snabbdom}---a performant virtual \gls{dom} library which was low-level enough for us to adapt it to our needs: integrating it with signals.
We actually \emph{fork} Snabbdom to make it compatible with our implementation of virtual nodes but, at its core, Snabbdom still provides us a single \gls{api} function: \code{patch}. This function takes two virtual nodes, the first one already \emph{rendered} on the actual \gls{dom} (\textit{i.e.} its structure represents an actual \gls{dom} structure) and a second one with the \emph{expected} structure for the \gls{dom} (the representation of what the \gls{dom} should become) and patches the actual \gls{dom} by diffing both virtual nodes and updating it accordingly.

\begin{figure}
  \includegraphics[width=\textwidth]{patch.png}
  \caption{Patching of virtual trees}
  \label{fig:patching}
\end{figure}

\Cref{fig:patching} showcases a patching between two arbitrary virtual trees where the coloured sections of the figure represent the \gls{dom} manipulations performed by the algorithm to go from the first tree to the second. As shown, the algorithm attempts to minimise the number of \gls{dom} mutations by only updating what effectively changed.

However, how does Loom allow signals to be used within \gls{html} values?
Conceptually, our virtual elements (when rendered) \emph{observe} any signals they may contain (in attributes or children); \code{patch}ing them against their previous value whenever the signals update. This approach allows us to only \code{patch} the sub-trees affected by a signal mutation.

As an example, consider the following Loom program and assume that \code{elem} has been rendered on the screen:
\begin{lstlisting}
var person = <i> "Alice"
var elem = <div> [
  <span> "Hello"
  mut person
]
elem.renderTo(document.getElementById("foo"))
*person = <i> "Bob"
\end{lstlisting}

\begin{figure}
  \centering
  \includegraphics[width=0.35\textwidth]{signals-patch.png}
  \caption{Sub-tree patching with signals}
  \label{fig:signal-patching}
\end{figure}

As shown in \cref{fig:signal-patching}, the signal update causes the sub-tree contained within the signal to be rerendered (using the \code{patch} function).

\section{Supporting CSS Values}
\label{sec:css-values}

This section goes over the last topic left to discuss: the implementation of \gls{css} values that follow the semantics defined in \cref{sec:first-class-css}.
With this in mind, consider the following example which showcases the creation of an interface with a reactive style sheet:
\begin{lstlisting}
var foo = mut "yellow"
var bar = css {display: "inline-block"}
var baz = css {
  color: foo
  backgroundColor: "green"
  |&#x > div.y:hover| {
    color: "blue"
  }
  ...bar
}
var x = <div#x {css: baz}> [
  "Hello",
  <div.y> ["World!"]
]
x.renderTo(document.getElementById("x"))
*foo = "purple"
\end{lstlisting}
This example creates and renders a \code{<div>} element with some children; the element has a style sheet applied to it. Note that this style sheet contains a signal (\code{foo}) within, associated with the \code{color} property.
Loom's compiler, given the above input, will output the following (\emph{prettified}) JavaScript code:
\begin{lstlisting}[language=js]
import Mut from "loom-lang/mutable"
import CSS from "loom-lang/css";
import CompSel from "loom-lang/composite-selector";
import SimpSel from "loom-lang/simple-selector";
import VElem from "loom-lang/virtual-element";
let foo = new Mut("yellow");
let bar = new CSS({kind: "properties",
                   properties: [["display", "inline-block"]]});
let baz = new CSS(
  {kind: "properties", properties: [["color", foo],
                                    ["backgroundColor", "green"]]},
  {kind: "rule", selectors: [
     new CompSel(">",
       new SimpSel({kind: "parent"}, {kind: "id", name: "x"}),
       new SimpSel({kind: "tag", name: "div"}, {kind: "class", name: "y"},
                   {kind: "pseudoClass", name: "hover", argument: null}))],
   body: new CSS({kind: "properties", properties: [["color", "blue"]]})},
  {kind: "import", imported: bar}
);
let x = new VElem("div", "x", [], {css: baz}, [
  "Hello"
  new VElem("div", null, ["y"], {}, ["World!"]),
]);
x.renderTo(document.getElementById("x"));
foo.setValue("purple");
\end{lstlisting}
Although this example is a bit harder to digest, it should not be difficult to map each of Loom's constructs to their JavaScript counterpart.
Once again, we use modules to aid with the compilation: in particular, we define classes for representing \gls{css} values and selectors.

\code{SimpSel} represents a \emph{simple selector}---a selector without combinators; \code{CompSel} represents a \emph{composite selector}---a composition of two selectors (the left one always \emph{simple}) via a combinator (in the example, the \code{>} combinator).

From this example, a few important questions arise: how to apply a style sheet to an element so that it becomes scoped to it? How to handle signals inside style sheets?

Typically, regarding the first question, frameworks that support the dynamic creation of style sheets render the whole style sheet in the global scope---though first prefixing each selector with a custom class (this is the case for frameworks such as Fela~\cite{fela} and Aphrodite~\cite{aphrodite}).
They then add said class to each element against which the style sheet should be applied.
If not being careful with the specificity of certain selectors, this may lead to an unwanted behaviour where rules of parent style sheets override more locally defined rules. As an example, consider the following Loom program:
\begin{lstlisting}
var outerCSS = css {
  |#red, #blue| {
    color: "blue"
  }
}
var innerCSS = css {
  color: "red"
}
var elem = <div#z {css: outerCSS}> [
  <div#red {css: innerCSS}> "Red"
  <div#blue> "Blue"
]
elem.renderTo(document.getElementById("z"))
\end{lstlisting}
If both \code{outerCSS} and \code{innerCSS} are rendered in the global scope, then the \gls{dom} would look something like the following, where both \code{<div>}s will be blue (because the outer rule is more specific than the inner one):
\begin{lstlisting}[language=html]
<!DOCTYPE html>
<html>
  <head>
    <style>
      .outer #red, .outer #blue {
        color: blue;
      }
      .inner {
        color: red;
      }
    </style>
  </head>
  <body>
    <div id="z" class="outer">
      <div id="red" class="inner">Red</div>
      <div id="blue">Blue</div>
    </div>
  </body>
</html>
\end{lstlisting}

These frameworks usually work around this issue by limiting the kind of selectors that may be used in their style sheets---at the expense of expressivity. Typically, they do not allow descendant selectors to be used.

In order to abide by the semantics designed in the previous chapter---which were purposely defined in a way that avoids any expressivity losses---in Loom, we reuse the idea of prefixing each selector with a class (though we use an attribute selector) together with an experimental technology named \emph{scoped styles}: which automatically have the scoping properties we desire.

In terms of the second question---how to handle signals inside style sheets---we take advantage of a recent \gls{css} feature called \emph{custom properties}, often referenced as \gls{css} variables. This feature enables the usage of variables within \gls{css}---variables whose values may be defined in elements. Most other existent frameworks require the rendering of \gls{css} to handle this sort of dynamism within their style sheets.

As such, the following is how the element identified by \code{x} in the first example should \emph{appear} in the \gls{dom} after being rendered:
\begin{lstlisting}[language=html]
<div id="x" data-loom-css-0 style="--loom-prop-0-0: yellow;">
  <style scoped>
    [data-loom-css-0] {
      color: var(--loom-prop-0-0);
      background-color: green;
      display: inline-block;
    }
    [data-loom-css-0]#x > div.y:hover {
      color: blue;
    }
  </style>
  Hello
  <div class="y">World!</div>
</div>
\end{lstlisting}
A mutation of the signal's value only requires the update of the \code{--loom-prop-0-0} custom attribute.

However, there are some issues with the proposed approach (which we intend to work on in the future; see also \cref{sec:future-directions}):
\begin{itemize}
\item Scoped styles are currently only supported in Firefox---with most other browsers seemingly not interested in implementing the feature---this means that scoped style sheets become available \emph{globally}. The web is moving in the direction of supporting \emph{web components} which \emph{do} support the scoping of styles---in the future, Loom may try to use this approach. Note that, whilst other browsers don't support the scoping of styles, most style sheets defined in Loom will still work properly: only certain rules with high-specificity selectors (such as the previously shown example) may \emph{override} rules defined more locally (this makes Loom behave alike to most existent solutions);

\item We place a \code{<style>} element on the children of \gls{html} elements with applied style sheets: this ruins the behaviour of certain \gls{css} selectors such as: \code{:first-child}, \code{:nth-child}, \textit{etc.};

\item Custom properties need to be polyfilled for compatibility with older browsers;

\item We have no efficient way of dealing with whole rules that are themselves signals such as \code{css \{|div| someSignal\}}: the mutation of this signal causes the rerendering of the whole style sheet.

  However, there are common situations where such rules are useful. As an example, think of the task manager application from \cref{cha:introduction}. We intend to style the text of a task if it is done; doing nothing when it is not done. This can be achieved via a \gls{css} value such as the following:
  \begin{lstlisting}
css {
  |.task-text| sig
    if (*(task.done))
      css {
        textDecoration: "line-through"
        color: "gray"
      }
    else
      css {}
}
  \end{lstlisting}
  Yet, as we have explained, the toggling of the \code{done} status of a task will cause the whole style sheet to rerender---which may be highly inefficient. Because this is such a common pattern, we introduce a syntactical construct for expressing the above showcased example---we introduce an \code{:if()} pseudo-selector:
  \begin{lstlisting}
css {
  |.task-text:if(task.done)| {
    textDecoration: "line-through"
    color: "gray"
  }
}
  \end{lstlisting}
  This pseudo-selector accepts either a boolean or a signal that evaluates to a boolean; its semantics are equivalent to those of the original formulation. However, this construct can be implemented efficiently. The following shows the structure of a done task, showcasing how the \code{if()} pseudo-selector translates to \gls{css}:
  \begin{lstlisting}[language=html]
<li class="task" data-loom-css-0 data-loom-if-1-0>
  <style scoped>
    [data-loom-if-1-0][data-loom-css-0] .task-text {
      text-decoration: line-through;
      color: gray;
    }
  </style>
  <input class="task-done" type="checkbox">
  <span class="task-text">Understand Loom's CSS values</span>
</li>
  \end{lstlisting}
  Toggling the \code{done} status of the task simply requires toggling the \code{data-loom-if-1-0} attribute of the element in order to update its presentation.
\end{itemize}

%==============================================================================%
% Playground                                                                   %
%==============================================================================%

\section{Loom's Playground}
\label{sec:loom-playground}

So that users may easily experiment with our language, we make available a playground to interact with Loom. This playground, itself implemented in Loom---with a few examples to choose from---may be found at: \url{https://loom-lang.gitlab.io/loom-playground}. Its source code may be found at: \url{https://gitlab.com/loom-lang/loom-playground}.

\Cref{fig:playground} displays the \gls{gui} of the playground. Note that the playground does not support the usage of multiple files; as such, examples which would normally be spread across multiple files are listed as a single one.

Further note that the page rendered on the right side is the result of an \code{export default} in the Loom program on the left.

\begin{figure}
  \includegraphics[width=\textwidth]{playground.png}
  \caption{Loom's Playground GUI}
  \label{fig:playground}
\end{figure}

