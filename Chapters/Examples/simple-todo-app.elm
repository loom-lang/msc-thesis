import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Json

main: Program Never Model Msg
main = Html.beginnerProgram {model = {nextId = 0, tasks = [],
                                      newTaskText = ""},
                             view = view, update = update}

-- MODEL
type alias Model = {nextId: Int, tasks: List Task, newTaskText: String }
type alias Task = {id: Int, text: String, done: Bool}

newTask: Int -> String -> Bool -> Task
newTask id text done = {id = id, text = text, done = done}

-- UPDATE
type Msg = UpdateNewTaskText String | AddTask | ToggleDone Int Bool

update: Msg -> Model -> Model
update msg model = case msg of
  UpdateNewTaskText text -> {model | newTaskText = text}
  AddTask -> {model | nextId = model.nextId + 1,
                      tasks = model.tasks ++
                              [newTask model.nextId model.newTaskText False],
                      newTaskText = ""}
  ToggleDone id done ->
    let updateTask task = if task.id == id then {task | done = done} else task
    in {model | tasks = List.map updateTask model.tasks}

-- VIEW
view: Model -> Html Msg
view model =
  div [id "todo-app"] [
    h1 [] [text "Todos"],
    input [class "new-task", placeholder "Add a task",
           value model.newTaskText, onInput UpdateNewTaskText,
           onEnter AddTask] [],
    ul [class "tasks-list"] <|
      List.map taskView model.tasks
  ]

taskView: Task -> Html Msg
taskView task =
  li [class "task"] [
    input [class "task-done", type_ "checkbox", checked task.done,
           onClick (ToggleDone task.id (not task.done))] [],
    span [class "task-text", style [("text-decoration",
            if task.done then "line-through" else "none")]]
      [text task.text]
  ]

onEnter msg = let isEnter code = if code == 13 then Json.succeed msg
                                 else Json.fail "Not enter"
              in on "keydown" (Json.andThen isEnter keyCode)
