%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Conclusions                                                                %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Conclusions}
\label{cha:conclusions}

The idea for Loom originated during the development of the client-side of some application in which many (dynamic) parts of the interface's presentation depended on application-data.
We were, at the time, working with JavaScript on the client-side and Node.js on the server-side, both using the same base language---we wondered whether it would be possible to do the same for \gls{html} and \gls{css}: keeping all the functionality of each language whilst benefitting from the expressivity of JavaScript.

As such, so far our efforts were put into making Loom work (we followed a very end-oriented approach): we wanted to validate our idea by having a working compiler for the language that allowed us to specify performant reactive interfaces in an easy manner---this is what we believe we have achieved.
We consider our implementation of the Loom compiler a very good start towards the premise of this dissertation: bridging client-side web technologies in a single programming language.
Our work will thus function as ground work for the many possible improvements that we follow up by enumerating.

%------------------------------------------------------------------------------%
% Future Directions                                                            %
%------------------------------------------------------------------------------%

\section{Future Directions}
\label{sec:future-directions}

However complete our implementation of the language's compiler may be, time was limited and, as such, there are plenty of improvements to be made (both to the compiler and to the language itself). The following list showcases what we believe will be part of Loom's future:
\begin{itemize}
% Formal definitions -----------------------------------------------------------
\item We intend to formally define the language: its basic constructs and behaviour regarding signals, \gls{html} and \gls{css} values, and the way they interact should be properly specified;

% Immutable data-types ---------------------------------------------------------
\item We will explore the usage of immutable data-types for representing lists, records, and other structures of importance---we are currently compiling them to JavaScript arrays and objects; with interoperability with JavaScript being one of our principles, this requires understanding how the JavaScript counterparts will be supported;

% Type system ------------------------------------------------------------------
\item We believe that a type system greatly improves the reliability of a language; as such, we intend to incorporate one in Loom: this includes a more in-depth study of existent type systems currently used in the context of the web to guarantee interoperability with JavaScript;

% Server-side ------------------------------------------------------------------
\item The server-side rendering of web pages is important; we will likely explore the usage of Loom in the server-side of an application---which would be possible by using Node.js.
This involves providing means for generating \gls{html} and \gls{css} documents on the fly from Loom's \gls{html} and \gls{css} values.
Other interesting ideas in this area involve the creation of seamless connections between client and server by means of signals and web sockets, achieving similar results to those of Meteor~\cite{meteor}.

% Push/pull signals ------------------------------------------------------------
\item Signals in Loom currently follow a \emph{pull}-based approach with regard to \emph{when} they update.
It is possible to optimise these updates---avoiding needless recomputations---by mixing both \emph{push} and \emph{pull} philosophies in a \emph{push-pull}-based approach.

% CSS improvements -------------------------------------------------------------
\item \gls{css} in Loom requires some tweaking: although it works, its performance should be further optimised for the case where we declare the body of a rule as a signal.
Another concern regards browser compatibility: currently, as previously mentioned, \gls{css} only works \emph{properly} in Firefox---albeit it's current behaviour is not exactly \emph{disastrous} in other browsers.
Obvious improvements to \gls{css} involve the straightforward implementation of certain features that are not currently in Loom simply because we did not have the time to do it: key-frame support and media queries being the two main ones.

% CSS types --------------------------------------------------------------------
\item Still in the topic of \gls{css}, we intend to explore better ways of integrating Loom with value-types supported by \gls{css}, i.e. support specifying units in numbers: such as \code{px}, \code{\%}, \code{em}, etc.
Once again, this is something that involves rethinking the language itself.

% Identify unused styles -------------------------------------------------------
\item Regarding \gls{css} and type systems, because we have both \gls{html} and \gls{css} values as first-class citizens in Loom, we may explore ways of identifying unused styles in style sheets---a problem in nowadays applications~\cite{redundant-css}.

% Better virtual DOM implementation --------------------------------------------
\item Although Loom's virtual \gls{dom} implementation should work well in practice, as shown in the benchmarks, there are still ways of making it better---especially because we have the freedom of compiling \gls{html} values in any way we see fit.
Libraries such as Inferno~\cite{inferno}---possibly the fastest currently existent virtual \gls{dom} library---take advantage of these kind of optimisations to achieve high levels of performance.

% Benchmarking -----------------------------------------------------------------
\item At last (but not least), we intend to benchmark Loom's \gls{css} values against both: virtual \gls{dom} libraries (to understand the impact of using ``\gls{css} in JS'' \textit{vs.} normal \gls{css} documents) and against other ``\gls{css} in JS'' libraries (\textit{e.g.} Fela---to compare implementation performance).
\end{itemize}
